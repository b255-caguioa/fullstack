let collection = [];

// Write the queue functions below.


function print() {
    return collection;
}

function enqueue(element) {
  collection[collection.length] = element;
  return collection;
}

function dequeue(element) {
  return collection.shift();
}

function front() {
    return collection[0];
}

function size() {
    return collection.length;
}

function isEmpty() {
    return collection.length = 0;
}



module.exports = {
    collection,
    print,
    enqueue,
    dequeue,
    front,
    size,
    isEmpty
};